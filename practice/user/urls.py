from django.contrib import admin
from user import views
from django.conf.urls import url
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^loan/', views.LoanGet.as_view()),
    url(r'^names/',views.NameGet.as_view()),

]