from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import Name,loan
from .serializer import name_serialiser,loan_serialiser
# Create your views here.




class NameGet(APIView):
    def get(self,request):

        Name_list=Name.objects.all()
        serialised=name_serialiser(Name_list,many='true')
        return Response(serialised.data)


class LoanGet(APIView):

    def get(self, request):
        loan_list = loan.objects.all()
        serialised = loan_serialiser(loan_list, many='true')
        return Response(serialised.data)
