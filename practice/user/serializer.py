from .models import Name,loan
from rest_framework import serializers

class name_serialiser(serializers.ModelSerializer):
    class Meta:
        model=Name
        fields='__all__'


class loan_serialiser(serializers.ModelSerializer):
    class Meta:
        model= loan
        fields= '__all__'

